# Exercise 2 - Find your own challenge-Exercises for ww44
#Python program to print the calendar of a given month and year.
#Write a Python program to get the current time

import calendar
y = int(input("Input the year : "))
m = int(input("Input the month : "))
print(calendar.month(y, m))

#Write a Python program to get the current time

print("This is the date and time of today:")
import datetime
dt = datetime.datetime.today().replace(microsecond=0)
print()
print(dt)
print()